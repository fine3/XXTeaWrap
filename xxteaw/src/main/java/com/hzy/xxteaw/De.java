package com.hzy.xxteaw;

import org.xxtea.XXTEA;

public class De {

    public static String en(String txt, String pwd) {
        String enc = XXTEA.encryptToBase64String(txt, pwd);
        return pwd + enc;
    }

    public static String de(String txt) {
        String src = txt.substring(4);
        String key = txt.substring(0, 4);
        return XXTEA.decryptBase64StringToString(src, key);
    }
}