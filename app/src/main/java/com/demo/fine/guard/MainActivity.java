package com.demo.fine.guard;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hzy.xxteaw.De;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String end = De.en("Hello World!!!我是中文", "yu&6");
        Log.e("TAG", end);
        String de = De.de(end);
        Log.e("TAG", de);
    }
}
